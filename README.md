# This is a "Workspace Container" for setting up a Kubernetes cluster with Azure Kubernetes Service (AKS) and Terraform

Article:

<https://geircode.atlassian.net/wiki/spaces/geircode/pages/421920769/2018-10-01+-+Create+a+Kubernetes+cluster+with+Azure+Kubernetes+Service+AKS+and+Terraform>

Usage:

- Run *Dockerfile.build.bat* first if you want to create your own Container Image.
- Run *docker-compose.up.bat* to start the workspace container