# python:3.6 is built on "Debian" 
# FROM python:3.6 
FROM geircode/setting_up_aks_base:latest

WORKDIR /app-scripts
COPY . /app-scripts
RUN find . -type f -print0 | xargs -0 dos2unix

WORKDIR /app
COPY . /app

ENTRYPOINT tail -f /dev/null