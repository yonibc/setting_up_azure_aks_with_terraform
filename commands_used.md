# Commands used in this setup

<https://geircode.atlassian.net/wiki/spaces/geircode/pages/421920769/2018-10-01+-+Create+a+Kubernetes+cluster+with+Azure+Kubernetes+Service+AKS+and+Terraform>

## Most important commands

```shell
find 00*.sh -type f -print0 | xargs -0 dos2unix
```

When you get the error:

```shell
$'pwd\r': command not found
convert_shell_scripts_to_unix.sh: line 4: $'\r': command not found
```

then run *dos2unix* on the file

## Create private/public key

Create the folder on the Windows host: 

```shell
../DockerSecrets/dockersecrets_1ae6cd4f/
```

Create a Azure VM with this public key:

```shell
ssh-keygen -t rsa -b 4096 -C your@email.no -f /ubuntu/ubuntu_rsa
```

and copy:
- ubuntu_rsa
- ubuntu_rsa.pub

to this folder "../DockerSecrets/dockersecrets_1ae6cd4f/"

## Login

```shell
dos2unix 000_chmod_secrets.sh
sh 000_chmod_secrets.sh
ssh -i /secrets/ubuntu_rsa garg@ubuntu-vscode.northeurope.cloudapp.azure.com
```

<https://cloudbase.it/freerdp-for-windows-nightly-builds/>

```shell
wfreerdp.exe /f /v:ubuntu-vscode.northeurope.cloudapp.azure.com:3389
```