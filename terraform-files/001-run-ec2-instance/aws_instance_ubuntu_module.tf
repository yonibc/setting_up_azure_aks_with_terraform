##################################################################################
# VARIABLES
##################################################################################

variable "shared_credentials_file" {
  default = "/var/run/secrets/aws_secrets_from_file" 
}
variable "ec2_private_key_file_path" {
  default = "/var/run/secrets/terraform_keypair_001_pem"
}
variable "ec2_public_key_name" {
  # the name of the public key pair in AWS, that is related to the private key in: "/var/run/secrets/terraform_keypair_001_pem"
  default = "terraform-keypair-001" 
}

##################################################################################
# PROVIDERS
##################################################################################

provider "aws" {
  region                  = "eu-west-1"
  shared_credentials_file =  "${var.shared_credentials_file}"
  profile                 = "terraform"
}

##################################################################################
# RESOURCES
##################################################################################

resource "aws_instance" "nginx" {
  ami           = "ami-06f42fd8c0931ce4b" # ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-20180814-3b73ef49-208f-47e1-8a6e-4ae768d8a333-ami-0b425589c7bb7663d.4
  instance_type = "t2.micro"
  key_name        = "${var.ec2_public_key_name}"

  connection {
    user        = "ubuntu"
    private_key = "${file("${var.ec2_private_key_file_path}")}" 
  }

  # testing inline exec
  # provisioner "remote-exec" {
  #   inline = [
  #     "sudo apt install zip"
  #   ]
  # }
}

data "aws_availability_zones" "available" {}

##################################################################################
# OUTPUT
##################################################################################

output "aws_instance_public_dns" {
    value = "${aws_instance.nginx.public_dns}"
}
