##################################################################################
# VARIABLES
##################################################################################

variable "shared_credentials_file" {
  default = "/var/run/secrets/aws_secrets_from_file" 
}
variable "ec2_private_key_file_path" {
  default = "/var/run/secrets/terraform_keypair_001_pem"
}
variable "ec2_public_key_name" {
  # the name of the public key pair in AWS, that is related to the private key in: "/var/run/secrets/terraform_keypair_001_pem"
  default = "terraform-keypair-001" 
}

##################################################################################
# PROVIDERS
##################################################################################

provider "aws" {
  region                  = "us-east-1"
  shared_credentials_file =  "${var.shared_credentials_file}"
  profile                 = "terraform"
}


data "aws_iam_role" "terraform_eks" {
  name = "terraform_eks"
}

data "aws_vpc" "vpc_eks" {
    tags {
      my-vpc-key = "vpc-eks"
  }
}

# looks like a function..
locals {
  # vpc_eks_id = "${data.aws_vpc.vpc_eks.id}"
  vpc_eks_id = "${data.aws_vpc.vpc_eks.id}"
}

## Only use these supported availability zones: => [us-east-1a, us-east-1c, us-east-1d]
data "aws_availability_zones" "available" {
}

data "aws_availability_zone" "us-east-1a" {
  name = "us-east-1a"
}
data "aws_availability_zone" "us-east-1c" {
  name = "us-east-1c"
}
data "aws_availability_zone" "us-east-1d" {
  name = "us-east-1d"
}

locals {
  availability_zone_0 = "${data.aws_availability_zone.us-east-1a.name}"
  availability_zone_1 = "${data.aws_availability_zone.us-east-1c.name}"
  availability_zone_2 = "${data.aws_availability_zone.us-east-1d.name}"
}

# locals {
#   availability_zone_0 = "${data.aws_availability_zones.available.names[0]}"
#   availability_zone_1 = "${data.aws_availability_zones.available.names[1]}"
#   availability_zone_2 = "${data.aws_availability_zones.available.names[2]}"
# }

data "aws_subnet" "vpc_eks__availability_zone_0" {
  availability_zone = "${local.availability_zone_0}"
  vpc_id = "${local.vpc_eks_id}"
}

data "aws_subnet" "vpc_eks__availability_zone_1" {
  availability_zone = "${local.availability_zone_1}"
  vpc_id = "${local.vpc_eks_id}"
}

data "aws_subnet" "vpc_eks__availability_zone_2" {
  availability_zone = "${local.availability_zone_2}"
  vpc_id = "${local.vpc_eks_id}"
}

resource "aws_eks_cluster" "example" {
  name     = "example"
  role_arn = "${data.aws_iam_role.terraform_eks.arn}"

  vpc_config {
    subnet_ids  = ["${data.aws_subnet.vpc_eks__availability_zone_0.id}", "${data.aws_subnet.vpc_eks__availability_zone_1.id}", "${data.aws_subnet.vpc_eks__availability_zone_2.id}"]
  }
}

output "endpoint" {
  value = "${aws_eks_cluster.example.endpoint}"
}

output "kubeconfig-certificate-authority-data" {
  value = "${aws_eks_cluster.example.certificate_authority.0.data}"
}
